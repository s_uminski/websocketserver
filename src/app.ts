const Koa = require('koa');
const IO = require('koa-socket-2');
const crypto = require("crypto");

async function initialize() {
  const app = new Koa();
  const io = new IO();

  const model = {
    first: {value: [], hash: crypto.randomBytes(32).toString('hex')},
    second: {value: [], hash: crypto.randomBytes(32).toString('hex')},
    third: {value: '', hash: crypto.randomBytes(32).toString('hex')},
  };
  io.attach(app);

  io.on('connection', (ctx) => {
    console.log('connection');
    ctx.emit('init', model);
  });
  io.on('update', (ctx, data) => {
    if (data.hash === model[data.key].hash) {
      model[data.key].value = data.value;
      model[data.key].hash = crypto.randomBytes(32).toString('hex');
      data.hash = model[data.key].hash;
      ctx.socket.broadcast.emit('update', data);
      ctx.socket.emit('ack', data);
    }
  });
  io.on('disconnect', (ctx) => {
    console.log('disconnect');
  });

  app.listen(3000);
  console.log(`App started, listening on 3000`);
}

initialize().catch(err => console.error(err));
